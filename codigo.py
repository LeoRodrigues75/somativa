# Trecho do código criado por Leonardo Jurado Rodrigues escrito para a disciplina Raciocínio Computacional

# Etapa 1
print("Olá, tudo bem?\n"
"Seja muito bem-vindo à Loja do Leonardo Jurado Rodrigues!\n"
"Vamos fazer uma análise de crédito.\n"
"Para começar, vamos precisar de alguns dados pessoais.")

cargo = str(input("Digite seu cargo atual:"))
salario = round(float(input("Digite seu salário atual:")), 2)
diaNasc = int(input("Digite o dia em que você nasceu:"))
mesNasc = int(input("Digite o mês em que você nasceu:"))
anoNasc = int(input("Digite o ano em que você nasceu:"))

print(f"Você é {cargo}, ganha R${salario} e nasceu em {diaNasc}/{mesNasc}/{anoNasc}.")